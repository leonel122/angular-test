import { Injectable } from '@angular/core';
import { Image } from '../models/image';

@Injectable()
export class ImageService {

  images: Image[] = [
    new Image('1','Primera Imagen','Description Primera Imagen','https://videotutoriales.com/maspa/maspa1.jpg','https://videotutoriales.com/maspa/maspa1-1.jpg'),
    new Image('2','Segunda Imagen','Description Segunda Imagen','https://videotutoriales.com/maspa/maspa2.jpg','https://videotutoriales.com/maspa/maspa2-1.jpg'),
    new Image('3','Tercera Imagen','Description Tercera Imagen','https://videotutoriales.com/maspa/maspa3.jpg','https://videotutoriales.com/maspa/maspa3-1.jpg'),
    new Image('4','Cuarta Imagen','Description Cuarta Imagen','https://videotutoriales.com/maspa/maspa4.jpg','https://videotutoriales.com/maspa/maspa4-1.jpg'),
    new Image('5','Quinta Imagen','Description Quinta Imagen','https://videotutoriales.com/maspa/maspa5.jpg','https://videotutoriales.com/maspa/maspa5-1.jpg'),
    new Image('6','Sexta Imagen','Description Sexta Imagen','https://videotutoriales.com/maspa/maspa6.jpg','https://videotutoriales.com/maspa/maspa6-1.jpg'),
    new Image('7','Septima Imagen','Description Septima Imagen','https://videotutoriales.com/maspa/maspa7.jpg','https://videotutoriales.com/maspa/maspa7-1.jpg'),
    new Image('8','Octaba Imagen','Description Octava Imagen','https://videotutoriales.com/maspa/maspa8.jpg','https://videotutoriales.com/maspa/maspa8-1.jpg'),
  ];

  constructor() {}
  
    getImages(){
      return this.images;
    }

}
