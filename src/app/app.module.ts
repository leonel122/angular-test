import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//se importa el servicio que se crea
import { ImageService } from './services/image.service';


import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { GaleryComponent } from './galery/galery.component';
import { ImageListComponent } from './galery/image-list/image-list.component';
import { ImageComponent } from './galery/image-list/image.component'


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    GaleryComponent,
    ImageListComponent,
    ImageComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ImageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
