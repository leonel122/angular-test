import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ng-galery',
  templateUrl: './galery.component.html',
  styleUrls: ['./galery.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GaleryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
