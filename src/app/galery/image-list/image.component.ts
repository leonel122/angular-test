import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Image } from '../../models/image';
@Component({
  selector: 'ng-image',
  templateUrl: './image.component.html',
  styles: [`img {
              box-shadow: 1px 1px 1px 1px gray;
              margin-bottom: 20px;
            }
            img:hover{
              filter: gray;
              -webkit-filter: grayscale(1);
            }
`],
  encapsulation: ViewEncapsulation.None
})
export class ImageComponent implements OnInit {
  @Input() image: Image;
  constructor() { }

  ngOnInit() {
  }

}
