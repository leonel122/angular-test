import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Image } from '../../services/image.service';

//importamos el servicio
import { ImageService } from '../../services/image.service';


@Component({
  selector: 'ng-image-list',
  templateUrl: './image-list.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None
})

export class ImageListComponent implements OnInit {
  images: Image[] = [];

  constructor(private imageService:ImageService) { }

  ngOnInit() {
    this.images = this.imageService.getImages();
  }

}
